package co;

public class HolaMundo {

	private String mensaje;
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public static void main(String[] args) {
		HolaMundo hola = new HolaMundo();
		hola.setMensaje("Hola Mundo");
		System.out.println(hola.getMensaje());
	}

}
